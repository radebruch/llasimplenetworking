# LLASimpleNetworking

Networking made easy with `LLASimpleNetworking`.

This package is NOT intended to replace the sophisticated solutions that come with other networking libraries like `Alamofire`.

Instead, it provides a lightweight, easy-to-understand framework for accessing simple APIs. It is not limited with regards to the supported protocols - but if the server returns anything other than JSON or plain text, you must implement the logic that parses the response yourself. In order to do so, take a look at `JsonResponse` and `StringResponse` to get an idea about how it works.

# Getting started
```swift
import Foundation
import PlaygroundSupport

/// USGS Earthquake count
///
/// Note that the struct is just declared to conform to `JsonResponse`.
/// That's all that's needed.
///
/// See: https://earthquake.usgs.gov/fdsnws/event/1/ for reference
struct QuakeCount: JsonResponse {
    let count: Int
    let maxAllowed: Int
    let error: String?
}

/// Query USGS Earthquake count
///
/// Note: The request class inherits from `BaseRequest`, specifying
///       the response type as generic parameter...
///
/// See: https://earthquake.usgs.gov/fdsnws/event/1/ for reference
class QuakeCountRequest: BaseRequest <QuakeCount> {
    private let dateFormat = { () -> DateFormatter in
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.dateFormat = "YYYY-MM-dd"
        return formatter
    }()

    let startDate: Date
    let endDate: Date
    let format = "geojson"

    init(days: UInt) {
        let secondsBack = TimeInterval(days * 24 * 60 * 60)
        startDate = Date.init(timeIntervalSinceNow: -secondsBack)
        endDate = Date()
    }

    /// Define the endpoint for this request
    override var endpoint: String {
        return "https://earthquake.usgs.gov/fdsnws/event/1/count"
    }

    /// That's how query string parameters are set up. No need to worry about
    /// encoding here - that's handled by the lib
    override var parameters: [(key: String, value: String?)] {
        return [
            (key: "starttime", value: dateFormat.string(from: startDate)),
            (key: "endtime", value: dateFormat.string(from: endDate)),
            (key: "format", value: format)
        ]
    }
}


// Send the request:

let daysIntoPast: UInt = 10

// Here we create the request
let request = QuakeCountRequest(days: daysIntoPast)

// And the network task
let task = NetworkTask(request: request)

// And here we send the request. The block will be called with the response
// asynchronously once the response was received.
task.start { result in
    switch result {
    case let .success(quakeCount):
        if let errText = quakeCount.error {
            print("*** The API reported and error:\n\(errText)\n")
        }
        print ("The quake count in the last \(daysIntoPast) days according to USGS was: \(quakeCount.count) (Limit: \(quakeCount.maxAllowed))")
    case let .failure(error):
        print("Received error: \(error) - \(error.localizedDescription)")
    }
}

PlaygroundPage.current.needsIndefiniteExecution = true
```


