task :default => :chores

#------------------------------------------------------------------------------------------------------------------------------------
desc "Format the Swift code in the project"
task :format do
	print "========== swiftformat #{`/usr/local/bin/swiftformat --version`.strip()} ==========\n"
	locations = " './Sources' './Tests'"
	system "/usr/local/bin/swiftformat --config .swiftformat" + locations
end

#------------------------------------------------------------------------------------------------------------------------------------
desc "Run the Realm SwiftLint tool"
task :lint do
	print "========== swiftlint #{`/usr/local/bin/swiftlint version`.strip()} ==========\n"
	system "/usr/local/bin/swiftlint"
end

#------------------------------------------------------------------------------------------------------------------------------------
desc "Run all chores"
task :chores => [:format, :lint]
