// MIT License
//
// Copyright © 2018-2020 Lutz Lameyer (radebruch /at/ gmail /dot/ com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import LLASimpleNetworking
import XCTest

final class RequestTests: XCTestCase {
    class TestRequest0: BaseRequest<String> {
        override var endpoint: String {
            return "http://www.example.com"
        }
    }

    final class TestRequest1: TestRequest0 {
        override var parameters: [(key: String, value: String?)] {
            return [
                (key: "name", value: "Horst-Jörg Johannes"),
                (key: "empty", value: ""),
                (key: "nix", value: nil),
            ]
        }

        override var headers: [(key: String, value: String)] {
            return [
                (key: "head1", value: "One"),
                (key: "multi", value: "mOne"),
                (key: "multi", value: "mTwo"),
                (key: "head2", value: "Two"),
            ]
        }

        override var body: Data? {
            return "HolleriDiDödelDu".data(using: .utf8)
        }

        override var timeout: TimeInterval {
            return 10
        }
    }

    func testDefaultValues() {
        let request = TestRequest0()

        XCTAssertEqual(request.acceptableStatusCodes, Array(200 ..< 300))
        XCTAssertEqual(request.headers.count, 0)
        XCTAssertEqual(request.parameters.count, 0)
        XCTAssertNil(request.body)
        XCTAssertEqual(request.timeout, 20.0, accuracy: 1e-4)
    }

    func testAsURLRequest() {
        var request: URLRequest?

        XCTAssertNoThrow(try TestRequest0().asURLRequest())
        request = try? TestRequest0().asURLRequest()

        XCTAssertNotNil(request)
        XCTAssertEqual(request?.url?.absoluteString, "http://www.example.com")
    }

    func testParams() {
        let request = TestRequest1()

        XCTAssertEqual(request.parameters.count, 3)

        let string = try? request.asURLRequest().url?.absoluteString
        XCTAssertNotNil(string)
        XCTAssert(checkParam("name=Horst-J%C3%B6rg%20Johannes", in: string))
        XCTAssert(checkParam("empty=", in: string))
        XCTAssert(checkParam("nix", in: string))
    }

    func testHeader() {
        let request = TestRequest1()
        let headers = try? request.asURLRequest().allHTTPHeaderFields ?? [:]

        XCTAssertNotNil(headers)
        XCTAssertEqual(headers?["head1"], "One")
        XCTAssertEqual(headers?["multi"], "mOne,mTwo")
        XCTAssertEqual(headers?["head2"], "Two")
    }

    func testBody() {
        let request = TestRequest1()
        let bodyData = try? request.asURLRequest().httpBody

        XCTAssertNotNil(bodyData)
        XCTAssertEqual(bodyData, "HolleriDiDödelDu".data(using: .utf8))
    }

    func testTimeout() {
        let request = TestRequest1()
        let timeout = try? request.asURLRequest().timeoutInterval

        XCTAssertNotNil(timeout)
        XCTAssertEqual(timeout, 10)
    }

    static var allTests = [
        ("testDefaultValues", testDefaultValues),
        ("testAsURLRequest", testAsURLRequest),
        ("testParams", testParams),
        ("testHeader", testHeader),
        ("testBody", testBody),
        ("testTimeout", testTimeout),
    ]

    // MARK: - private helper

    private func checkParam(_ parm: String, in string: String?) -> Bool {
        let single = string?.hasSuffix("?\(parm)") == true
        let atStart = string?.contains("?\(parm)&") == true
        let inMiddle = string?.contains("&\(parm)&") == true
        let atEnd = string?.hasSuffix("&\(parm)") == true

        return single || atStart || inMiddle || atEnd
    }
}
