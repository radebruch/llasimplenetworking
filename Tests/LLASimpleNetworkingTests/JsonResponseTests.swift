// MIT License
//
// Copyright © 2018-2020 Lutz Lameyer (radebruch /at/ gmail /dot/ com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import LLASimpleNetworking
import XCTest

final class JsonResponseTests: XCTestCase {
    struct TestStruct: JSONResponse {
        let firstName: String
        let lastName: String
        let numberOfEyes: Int
    }

    let testJsonData = #"{"firstName":"Horst","lastName":"Juppkötter","numberOfEyes":2}"# .data(using: .utf8)
    let testResponse: URLResponse? = HTTPURLResponse(
        url: URL(string: "https://www.example.com")!,
        statusCode: 200,
        httpVersion: "HTTP/1.1",
        headerFields: ["Content-Type": "application/json"])

    func testCreateJsonResponse() {
        let resp = try? TestStruct.create(from: testJsonData, urlResponse: testResponse)

        XCTAssertEqual(resp?.firstName, "Horst")
        XCTAssertEqual(resp?.lastName, "Juppkötter")
        XCTAssertEqual(resp?.numberOfEyes, 2)
    }

    func testCreateEmptyResponse() {
        XCTAssertThrowsError(try { _ = try TestStruct.create(from: Data(), urlResponse: self.testResponse) }())
    }

    static var allTests = [
        ("testCreateJsonResponse", testCreateJsonResponse),
        ("testCreateEmptyResponse", testCreateEmptyResponse),
    ]
}
