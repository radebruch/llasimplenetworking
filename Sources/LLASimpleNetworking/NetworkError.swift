// MIT License
//
// Copyright © 2018-2020 Lutz Lameyer (radebruch /at/ gmail /dot/ com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import Foundation

/// A struct representing different types of network errors.
///
/// This was deliberately designed as a struct so client apps using the library can extend this type with their own domain
/// specific errors.
public struct NetworkError: Error, CustomStringConvertible, Equatable {
    private let type: String

    public var description: String {
        return type
    }

    /// Not really an error; this is the result cancelling the networkTask while underway
    public static let cancelled = NetworkError(type: "Request cancelled")

    /// The endpoint provided by the `Request` could not be used to create a `URL`.
    public static let endpointUnusable = NetworkError(type: "Endpoint unusable")
    /// The `Request` could not be converted into a `URLRequest`.
    public static let invalidRequest = NetworkError(type: "Invalid request")
    /// The response body was empty unexpectedly.
    public static let unexpectedEmptyResponse = NetworkError(type: "Unexpected empty response")
    /// An error occurred while parsing the response.
    public static let unparsableResponse = NetworkError(type: "Unparsable response")
    /// The status code received in the server response doesn't match the acceptable status codes defined in the `Request`
    public static let unacceptableStatusCode = NetworkError(type: "Unacceptable status code")
}
