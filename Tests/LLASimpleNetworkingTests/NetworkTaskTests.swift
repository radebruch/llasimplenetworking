// MIT License
//
// Copyright © 2018-2020 Lutz Lameyer (radebruch /at/ gmail /dot/ com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import LLALogging
@testable
import LLASimpleNetworking
import XCTest

final class NetworkTaskTests: XCTestCase {
    enum DummyError: Error {
        case void
    }

    struct TestStruct: JSONResponse {
        let firstName: String
        let lastName: String
        let numberOfEyes: Int
    }

    // swiftlint:disable identifier_name
    class TestJsonRequest: BaseRequest<TestStruct> {
        var _session: URLSession!

        override var session: URLSession {
            return _session
        }

        override var endpoint: String {
            return "http://www.example.com"
        }
    }

    class TestStringRequest: BaseRequest<String> {
        var _session: URLSession!

        override var session: URLSession {
            return _session
        }

        override var endpoint: String {
            return "http://www.example.com"
        }
    }

    // swiftlint:enable identifier_name

    let test200Data = #"{"firstName":"Horst","lastName":"Juppkötter","numberOfEyes":2}"# .data(using: .utf8)
    let test204Data: Data? = nil
    let test404Data = #"<html><head><title>Resource not found</title></head><body><H1>404 Not Found</H1><p>The resource you requested could not be found on this server, sorry.</p></body></html>"# .data(using: .utf8)

    let testResponse200: URLResponse? = HTTPURLResponse(
        url: URL(string: "https://www.example.com")!,
        statusCode: 200,
        httpVersion: "HTTP/1.1",
        headerFields: ["Content-Type": "application/json"])

    let testResponse204: URLResponse? = HTTPURLResponse(
        url: URL(string: "https://www.example.com")!,
        statusCode: 204,
        httpVersion: "HTTP/1.1",
        headerFields: [:])

    let testResponse404: URLResponse? = HTTPURLResponse(
        url: URL(string: "https://www.example.com")!,
        statusCode: 404,
        httpVersion: "HTTP/1.1",
        headerFields: ["Content-Type": "text/html; charset=utf-8"])

    override func setUp() {
        LogManager.shared.logLevel = .verbose
    }

    func testNetworkTaskNoError() {
        let session = MockSession(data: test200Data, response: testResponse200, error: nil)
        let req = TestJsonRequest()
        req._session = session
        let task = NetworkTask(request: req)

        let responseReceived = XCTestExpectation(description: "Response received")
        var result: Result<TestStruct?, Error>?

        task.start { res in
            result = res
            responseReceived.fulfill()
        }

        wait(for: [responseReceived], timeout: 1.0)
        guard let res = result else { XCTFail("nil result unexpected"); return }
        switch res {
        case let .success(response):
            XCTAssertNotNil(response)
        case .failure:
            XCTFail("Received .failure when .success was expected")
        }
    }

    func testNetworkTask204() {
        let session = MockSession(data: test204Data, response: testResponse204, error: nil)
        let req = TestStringRequest()
        req._session = session
        let task = NetworkTask(request: req)

        let responseReceived = XCTestExpectation(description: "Response received")
        var result: Result<String?, Error>?

        task.start { res in
            result = res
            responseReceived.fulfill()
        }

        wait(for: [responseReceived], timeout: 1.0)
        guard let res = result else { XCTFail("nil result unexpected"); return }
        switch res {
        case let .success(response):
            XCTAssertNil(response)
        case .failure:
            XCTFail("Received .failure when .success was expected")
        }
    }

    func testNetworkTask404() {
        let session = MockSession(data: test404Data, response: testResponse404, error: nil)
        let req = TestJsonRequest()
        req._session = session
        let task = NetworkTask(request: req)

        let responseReceived = XCTestExpectation(description: "Response received")
        var result: Result<TestStruct?, Error>?

        task.start { res in
            result = res
            responseReceived.fulfill()
        }

        wait(for: [responseReceived], timeout: 1.0)
        guard let res = result else { XCTFail("nil result unexpected"); return }
        switch res {
        case .success:
            XCTFail("Received .success when .failure was expected")
        case let .failure(err):
            XCTAssertNotNil(err)
        }
    }

    func testNetworkTaskCancelled() {
        let cancelErr = NSError(domain: NSURLErrorDomain, code: NSURLErrorCancelled, userInfo: nil)
        let session = MockSession(data: test200Data, response: nil, error: cancelErr)
        let req = TestJsonRequest()
        req._session = session
        let task = NetworkTask(request: req)

        let responseReceived = XCTestExpectation(description: "Response received")
        var result: Result<TestStruct?, Error>?

        task.start { res in
            result = res
            responseReceived.fulfill()
        }

        wait(for: [responseReceived], timeout: 1.0)
        guard let res = result else { XCTFail("nil result unexpected"); return }
        switch res {
        case .success:
            XCTFail("Received .success when .failure was expected")
        case let .failure(err):
            XCTAssert(err is NetworkError)
            XCTAssertEqual(err as? NetworkError, NetworkError.cancelled)
        }
    }

    static var allTests = [
        ("testNetworkTaskNoError", testNetworkTaskNoError),
        ("testNetworkTask204", testNetworkTask204),
        ("testNetworkTask404", testNetworkTask404),
        ("testNetworkTaskCancelled", testNetworkTaskCancelled),
    ]
}
