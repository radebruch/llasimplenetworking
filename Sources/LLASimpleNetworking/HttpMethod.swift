// MIT License
//
// Copyright © 2018-2020 Lutz Lameyer (radebruch /at/ gmail /dot/ com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

/// HTTP/1.1 request methods
///
/// - get: requests a representation of the specified resource.
/// - head: asks for a response identical to that of a GET request, but without the response body.
/// - put: requests that the enclosed entity be stored under the supplied URI.
/// - post: requests that the server accept the entity enclosed in the request as a new subordinate
///         of the web resource identified by the URI.
/// - delete: deletes the specified resource.
/// - connect: converts the request connection to a transparent TCP/IP tunnel, usually to
///            facilitate SSL-encrypted communication (HTTPS) through an unencrypted HTTP proxy.
/// - options: returns the HTTP methods that the server supports for the specified URL.
/// - trace: echoes the received request so that a client can see what (if any) changes or additions
///          have been made by intermediate servers.
/// - patch: applies partial modifications to a resource.
public enum HttpMethod: String {
    case get = "GET"
    case head = "HEAD"
    case put = "PUT"
    case post = "POST"
    case delete = "DELETE"
    case connect = "CONNECT"
    case options = "OPTIONS"
    case trace = "TRACE"
    case patch = "PATCH"
}
