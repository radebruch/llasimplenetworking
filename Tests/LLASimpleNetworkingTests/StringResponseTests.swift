// MIT License
//
// Copyright © 2018-2020 Lutz Lameyer (radebruch /at/ gmail /dot/ com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import LLASimpleNetworking
import XCTest

final class StringResponseTests: XCTestCase {
    func testCreateString() {
        let resp1 = HTTPURLResponse(
            url: URL(string: "https://www.example.com")!,
            mimeType: "text/plain",
            expectedContentLength: 240,
            textEncodingName: "ISO-8859-1") // correct charset, explicitly

        let resp2 = HTTPURLResponse(
            url: URL(string: "https://www.example.com")!,
            mimeType: "text/plain",
            expectedContentLength: 240,
            textEncodingName: "UTF-8") // wrong chatset

        let resp3 = HTTPURLResponse(
            url: URL(string: "https://www.example.com")!,
            statusCode: 200,
            httpVersion: "HTTP/1.1",
            headerFields: ["Content-Type": "text/plain; charset=iso-8859-1"]) // correct charset via Content-Type

        let originalString = "Pfeift der Sturm? Keift ein Wurm? Heulen Eulen hoch vom Turm? Nein! Es ist des Galgenstrickes dickes Ende, welches ächzte, gleich als ob im Galopp eine müdgehetzte Mähre nach dem nächsten Brunnen lechzte (der vielleicht noch ferne wäre)."
        let data = originalString.data(using: .isoLatin1)

        let string1: String? = try? String.create(from: data, urlResponse: resp1)
        let string2: String? = try? String.create(from: data, urlResponse: resp2)
        let string3: String? = try? String.create(from: data, urlResponse: resp3)

        XCTAssertEqual(string1, originalString)
        XCTAssertNotEqual(string2, originalString)
        XCTAssertEqual(string3, originalString)
    }

    func testCreateEmptyString() {
        let resp = HTTPURLResponse(
            url: URL(string: "https://www.example.com")!,
            statusCode: 200,
            httpVersion: "HTTP/1.1",
            headerFields: ["Content-Type": "text/plain; charset=utf-8"]) // correct charset via Content-Type

        let originalString = ""
        let data = originalString.data(using: .utf8)

        let string: String? = try? String.create(from: data, urlResponse: resp)

        XCTAssertEqual(string, originalString)
    }

    static var allTests = [
        ("testCreateString", testCreateString),
        ("testCreateEmptyString", testCreateEmptyString),
    ]
}
