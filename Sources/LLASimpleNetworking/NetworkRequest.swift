// MIT License
//
// Copyright © 2018-2020 Lutz Lameyer (radebruch /at/ gmail /dot/ com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import Foundation

/// Implementors represent network requests.
public protocol NetworkRequest {
    /// The HTTP method for this request.
    var method: HttpMethod { get }
    /// The content type for this request.
    ///
    /// If this is not `nil` and a `body` is present (not `nil`),
    /// then this overrules a potential `Content-Type` entry in the `headers` array.
    var contentType: String? { get }
    /// An array of integers representing HTTP status code that are acceptable
    /// in the server response.
    ///
    /// If the framework receives a status code that is not listed here it will
    /// throw a `NetworkError.unacceptableStatusCode`
    var acceptableStatusCodes: [Int]? { get }
    /// The URL string where the request will be sent.
    var endpoint: String { get }
    /// The request headers.
    var headers: [(key: String, value: String)] { get }
    /// The request parameters. Keys and values can be plain strings. The framework
    /// will automatically take care of
    /// properly encoding non-ascii characters.
    var parameters: [(key: String, value: String?)] { get }
    /// The request body.
    var body: Data? { get }
    /// The network timeout in seconds.
    ///
    /// If a response is not received after this time interval, the request will fail
    /// with a network timeout error.
    var timeout: TimeInterval { get }
    /// The `URLSession` that shall be used for this request.
    var session: URLSession { get }

    /// The type of the response
    associatedtype ResponseType: NetworkResponse
}

// MARK: - Convert into an URLRequest

public extension NetworkRequest {
    /// Create a new `URLRequest` instance from this request.
    ///
    /// - Returns: a `URLRequest` representing this request entity, with all the
    ///            properties set appropriately according to the variables defined
    ///            in the `NetworkRequest` protocol.
    /// - Throws: an `Error` if the conversion fails for some reason. (Usually
    ///           invalid endpoint or parameters).
    func asURLRequest() throws -> URLRequest {
        guard var components = URLComponents(string: endpoint) else {
            throw NetworkError.endpointUnusable
        }

        // add parameters (if any)
        let items = parameters.map { URLQueryItem(name: $0.key, value: $0.value) }
        if items.count > 0 {
            components.queryItems = items
        }

        guard let url = components.url else {
            throw NetworkError.invalidRequest
        }

        var request = URLRequest(url: url)

        // method
        request.httpMethod = method.rawValue

        // headers
        for (fieldName, value) in headers {
            request.addValue(value, forHTTPHeaderField: fieldName)
        }

        // body
        if body != nil {
            if let contentType = contentType {
                request.allHTTPHeaderFields?["Content-Type"] = contentType
            }
        }
        request.httpBody = body

        // timeout
        request.timeoutInterval = timeout

        return request
    }
}

// MARK: - Default values where reasonable

/// A base class providing some default values for most of the variables.
///
/// Sub classes MUST specify the associated `ResponseType` and override `endpoint`.
/// All the other properties MAY be overridden as well, as appropriate.
open class BaseRequest<R>: NetworkRequest where R: NetworkResponse {
    public typealias ResponseType = R

    public init() {}

    /// The HTTP method for this request. Will be `GET` if not overridden.
    open var method: HttpMethod {
        return .get
    }

    /// The content type. Will be `nil` if not overridden.
    open var contentType: String? {
        return nil
    }

    /// The endpoint for this request. Sub classes MUST override this variable and
    /// return the appropriate endpoint!
    open var endpoint: String {
        preconditionFailure("endpoint MUST be defined in sub class")
    }

    /// The acceptable status codes. This will be any code in the range 200...299 if not
    /// overridden.
    open var acceptableStatusCodes: [Int]? {
        return Array(200 ..< 300)
    }

    /// The request headers. Will be an empty array if not overridden.
    open var headers: [(key: String, value: String)] {
        return []
    }

    /// The request parameters. Will be an empty array if not overridden.
    open var parameters: [(key: String, value: String?)] {
        return []
    }

    /// The request body data. Will be `nil` if not overridden.
    open var body: Data? {
        return nil
    }

    /// The timeout interval in seconds. Will be `20s` if not overridden.
    open var timeout: TimeInterval {
        return 20.0 /* sec */
    }

    /// The session for this request (defaults to `URLSession.shared`)
    open var session: URLSession {
        return URLSession.shared
    }
}
