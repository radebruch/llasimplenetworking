// MIT License
//
// Copyright © 2018-2020 Lutz Lameyer (radebruch /at/ gmail /dot/ com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import Foundation
import LLALogging

/// Handles sending requests and receiving responses.
///
/// This is the key class in the framework! Crate a new instance of `NetworkTask` with the
/// request that you want to send. Then call `start` and the request goes to the server.
/// The response will then be delivered asynchronously on the main thread once it arrives.
public final class NetworkTask<RequestType> where RequestType: NetworkRequest {
    private let request: RequestType
    private var task: URLSessionDataTask?
    private var startTime: Date!
    private let traceId: String
    private var log = LogManager.shared.createLogger()

    /// Create a new instance.
    ///
    /// - Parameters:
    ///   - request: The request that shall be sent by this task.
    public init(request: RequestType) {
        self.request = request
        // for logging only
        func randomSyllable() -> String {
            return "\("bcdfghjklmnpqrstvwxyz".randomElement()!)\("aeiouy".randomElement()!)"
        }
        traceId = randomSyllable() + randomSyllable() + randomSyllable() + randomSyllable()
    }

    /// Start the task.
    ///
    /// The request will be sent when this function is called. The function returns immediately. The result of the
    /// network task will be delivered later by calling the `responseHandler` callback.
    ///
    /// - Parameters:
    ///   - responseHandler: The response handler callback. The function will make sure that all callbacks are
    ///                      delivered on the main thread so UI changes can be made directly in the callback body.
    ///   - result: The result of the network task.
    public func start(responseHandler: @escaping (_ result: Result<RequestType.ResponseType?, Error>) -> Void) {
        startTime = Date()
        func callback(_ response: RequestType.ResponseType?, _ error: Error?) {
            let duration = -startTime.timeIntervalSinceNow
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.log.debug("<\(self.traceId)> Task completed after \(String(format: "%.3f", duration))s. Error: \(error?.localizedDescription ?? "nil")")
                if let error = error {
                    responseHandler(.failure(error))
                } else {
                    responseHandler(.success(response))
                }
            }
        }

        func handleSuccess(_ data: Data?, _ response: URLResponse?) throws {
            let resp = try RequestType.ResponseType.create(from: data, urlResponse: response)
            callback(resp, nil)
        }

        func checkResponseCodeIfApplicable(_ httpResponse: HTTPURLResponse) throws {
            if request.acceptableStatusCodes?.contains(httpResponse.statusCode) == false {
                throw NetworkError.unacceptableStatusCode
            }
        }

        func taskCompletionHandler(_ data: Data?, _ response: URLResponse?, _ error: Error?) throws {
            task = nil
            if let error = error {
                let nserror = error as NSError
                if nserror.domain == NSURLErrorDomain, nserror.code == NSURLErrorCancelled {
                    throw NetworkError.cancelled
                }
                throw error
            }
            log.verbose("<\(traceId)> Response details:\n\(response?.extendedDescription() ?? "nil")")
            if let data = data, data.count > 0 {
                log.verbose(data)
            }
            if let httpResponse = response as? HTTPURLResponse {
                try checkResponseCodeIfApplicable(httpResponse)
                if httpResponse.statusCode == 204, data == nil || data?.count == 0 {
                    callback(nil, nil)
                    return
                }
            }
            try handleSuccess(data, response)
        }

        do {
            let urlRequest = try request.asURLRequest()
            task = request.session.dataTask(with: urlRequest) { data, response, err in
                do {
                    try taskCompletionHandler(data, response, err)
                } catch {
                    callback(nil, error)
                }
            }
            log.debug("<\(traceId) Now sending request: \(type(of: request))")
            log.verbose("<\(traceId)> Request details:\n\(urlRequest.extendedDescription())")
            task?.resume()
        } catch {
            callback(nil, error)
        }
    }

    /// Check if the task is currently active (i.e. waiting for a pending response after a request was sent).
    public var active: Bool {
        return task != nil
    }

    /// Cancel the request.
    ///
    /// The response callback will be called with error: `NetworkError.cancelled`.
    public func cancel() {
        task?.cancel()
    }
}
