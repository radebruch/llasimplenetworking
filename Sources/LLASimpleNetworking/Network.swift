// MIT License
//
// Copyright © 2018-2020 Lutz Lameyer (radebruch /at/ gmail /dot/ com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import Foundation
import SystemConfiguration

/// Check if the network is available / reachable.
///
/// Heavily inspired by this excellent post authored by Marco Santa:
/// https://marcosantadev.com/network-reachability-swift
public final class Network {
    /// This notification will be posted when the network reachability changes.
    public static let ConnectivityChangedNotification = Notification.Name(rawValue: "network.connectivity.changed")
    /// The singleton instance of this class.
    public static let `default` = Network()

    private var reachability: SCNetworkReachability!
    private var lastReachableValue: Bool?

    private init() {
        var zero = sockaddr_in()
        zero.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zero.sin_family = sa_family_t(AF_INET)

        reachability = withUnsafePointer(to: &zero) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }
        if reachability == nil { preconditionFailure("unable to create reachability") }
        setupNotifications()
    }

    /// Returns the value `true` if the network is reachable.
    public var connected: Bool {
        var flags: SCNetworkReachabilityFlags = []
        let ok = SCNetworkReachabilityGetFlags(reachability, &flags)
        if !ok { return false }
        return reachable(with: flags)
    }

    private func reachable(with flags: SCNetworkReachabilityFlags) -> Bool {
        let isReachable = flags.contains(.reachable)
        let isConnected = !flags.contains(.connectionRequired)
        let isAutoConnecting = flags.contains(.connectionOnDemand) || flags.contains(.connectionOnTraffic)
        let canAutoConnectSilently = isAutoConnecting && !flags.contains(.interventionRequired)

        return isReachable && (isConnected || canAutoConnectSilently)
    }

    private func setupNotifications() {
        let callback: SCNetworkReachabilityCallBack = { _, flags, info in
            guard let info = info else { assertionFailure("handler instance not found"); return }
            let handler = Unmanaged<Network>.fromOpaque(info).takeUnretainedValue()

            var flags: SCNetworkReachabilityFlags = []
            let ok = SCNetworkReachabilityGetFlags(handler.reachability, &flags)
            let newReachable: Bool
            if ok {
                newReachable = handler.reachable(with: flags)
            } else {
                newReachable = false
            }
            if handler.lastReachableValue != newReachable {
                handler.lastReachableValue = newReachable
                DispatchQueue.main.async {
                    let payload = ConnectivityChangedNotificationPayload(connected: newReachable)
                    NotificationCenter.default.post(payload.notification(with: handler))
                }
            }
        }

        var context = SCNetworkReachabilityContext(version: 0, info: nil, retain: nil, release: nil, copyDescription: nil)
        context.info = UnsafeMutableRawPointer(Unmanaged<Network>.passUnretained(self).toOpaque())

        if !SCNetworkReachabilitySetCallback(reachability, callback, &context) {
            assertionFailure("Unable to set up reachability callback (no notifications available!)")
        }

        if !SCNetworkReachabilitySetDispatchQueue(reachability, DispatchQueue.global(qos: .background)) {
            assertionFailure("Unable to set up callback queue (no notifications available!")
        }
    }
}

/// This structure represents the notification payload of the `ConnectivityChangedNotification` posted by the `Network` class
/// if the connectivity changes.
public struct ConnectivityChangedNotificationPayload {
    private static let ConnectedKey = "connected"
    /// The value of the `connected` property from the notification. Will be `true` if the notification was sent after the network became available,
    /// `false` if the notification was posted after the network became unavailable.
    public let connected: Bool

    /// Create a new payload struct from the given notification. (The initializer will fail, i.e. return `nil`, if the notification is not a `ConnectivityChangedNotification`.)
    /// - Parameter notification: the `ConnectivityChangedNotification` from which the payload shall be represented.
    public init?(_ notification: Notification) {
        guard
            notification.name == Network.ConnectivityChangedNotification,
            let connected = notification.userInfo?[ConnectivityChangedNotificationPayload.ConnectedKey] as? Bool
        else {
            return nil
        }

        self.connected = connected
    }

    fileprivate init(connected: Bool) {
        self.connected = connected
    }

    fileprivate func notification(with object: Any? = nil) -> Notification {
        let userInfo = [ConnectivityChangedNotificationPayload.ConnectedKey: connected]
        return Notification(name: Network.ConnectivityChangedNotification, object: object, userInfo: userInfo)
    }
}
